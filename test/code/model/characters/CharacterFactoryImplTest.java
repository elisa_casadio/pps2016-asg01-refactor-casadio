package code.model.characters;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Elisa on 16/03/2017.
 */
public class CharacterFactoryImplTest {

    private static final int POSITION_X = 10;
    private static final int POSITION_Y = 10;

    private Character characterActual;
    private Character characterExpected;

    @Test
    public void createMario() {
        this.characterActual = new Mario(POSITION_X, POSITION_Y);
        this.characterExpected = new CharacterFactoryImpl().createMario(POSITION_X, POSITION_Y);
        assertEquals(listOfFields(this.characterActual), listOfFields(this.characterExpected));
    }

    private List<Integer> listOfFields(Character character) {
        List<Integer> list = new ArrayList<>();
        list.add(character.getX());
        list.add(character.getY());
        list.add(character.getHeight());
        list.add(character.getWidth());
        return list;
    }

    @Test
    public void createMushroom() {
        this.characterActual = new Mushroom(POSITION_X, POSITION_Y);
        this.characterExpected = new CharacterFactoryImpl().createMushroom(POSITION_X, POSITION_Y);
        assertEquals(listOfFields(this.characterActual), listOfFields(this.characterExpected));
    }

    @Test
    public void createTurtle() {
        this.characterActual = new Turtle(POSITION_X, POSITION_Y);
        this.characterExpected = new CharacterFactoryImpl().createTurtle(POSITION_X, POSITION_Y);
        assertEquals(listOfFields(this.characterActual), listOfFields(this.characterExpected));
    }

}