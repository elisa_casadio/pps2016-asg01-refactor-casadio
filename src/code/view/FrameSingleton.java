package code.view;

import javax.swing.*;

/**
 * Created by Elisa on 17/03/2017.
 */
public class FrameSingleton {
    private static final String WINDOW_TITLE = "Super Mario";

    private static JFrame FRAME = null;

    private FrameSingleton() {}

    public static JFrame getFrame() {
        if (FRAME == null) {
            synchronized (FrameSingleton.class) {
                if (FRAME == null) {
                    FRAME = new JFrame(WINDOW_TITLE);
                }
            }
        }
        return FRAME;
    }
}
