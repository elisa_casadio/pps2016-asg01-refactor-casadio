package code.model.objects;

import java.awt.Image;

import code.view.Platform;

public class GameObjectImpl implements GameObject {

    private int width, height;
    private int x, y;

    private Image imageObject;

    public GameObjectImpl(int x, int y, int width, int height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    @Override
    public int getWidth() {
        return width;
    }

    @Override
    public int getHeight() {
        return height;
    }

    @Override
    public int getX() {
        return x;
    }

    @Override
    public int getY() {
        return y;
    }

    @Override
    public Image getImgObj() {
        return imageObject;
    }

    @Override
    public void setWidth(int width) {
        this.width = width;
    }

    @Override
    public void setHeight(int height) {
        this.height = height;
    }

    @Override
    public void setX(int x) {
        this.x = x;
    }

    @Override
    public void setY(int y) {
        this.y = y;
    }

    @Override
    public void setImageObject(Image imageObject) {
        this.imageObject = imageObject;
    }

    @Override
    public void move() {
        if (Platform.getPlatform().getxPos() >= 0) {
            this.x = this.x - Platform.getPlatform().getMov();
        }
    }

}
