package code.model.objects;

import code.utils.Res;
import code.utils.Utils;

public class Block extends GameObjectImpl {

    public static final int WIDTH = 30;
    public static final int HEIGHT = 30;

    public Block(int x, int y) {
        super(x, y, WIDTH, HEIGHT);
        setImageObject(Utils.getImage(Res.IMG_BLOCK));
    }

}
