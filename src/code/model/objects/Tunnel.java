package code.model.objects;

import code.utils.Res;
import code.utils.Utils;

public class Tunnel extends GameObjectImpl {

    public static final int WIDTH = 43;
    public static final int HEIGHT = 65;

    public Tunnel(int x, int y) {
        super(x, y, WIDTH, HEIGHT);
        setImageObject(Utils.getImage(Res.IMG_TUNNEL));
    }

}
