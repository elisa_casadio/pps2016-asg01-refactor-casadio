package code.model.objects;

import java.awt.*;

/**
 * Created by Elisa on 16/03/2017.
 */
public interface GameObject {

    int getWidth();

    int getHeight();

    int getX();

    int getY();

    Image getImgObj();

    void setWidth(int width);

    void setHeight(int height);

    void setX(int x);

    void setY(int y);

    void setImageObject(Image imageObject);

    void move();
}
