package code.model.characters;

import code.model.objects.GameObject;

/**
 * Created by Elisa on 12/03/2017.
 */
public interface CharacterStrategy {

    /**
     * Set acton when character moves.
     */
    void move();

    /**
     * Set action when character contacts character.
     * @param character a character
     */
    void contactCharacter(BasicCharacter character);

    /**
     * Set action when character contacts object.
     * @param object a game object
     */
    void contactObject(GameObject object);
}
