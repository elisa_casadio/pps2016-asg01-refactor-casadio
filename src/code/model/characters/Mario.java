package code.model.characters;

import java.awt.Image;

import code.model.objects.GameObject;
import code.model.objects.Piece;
import code.utils.Res;
import code.utils.Utils;
import code.view.Platform;

public class Mario extends BasicCharacter implements CharacterStrategy {

    private static final int MARIO_OFFSET_Y_INITIAL = 243;
    private static final int FLOOR_OFFSET_Y_INITIAL = 293;
    private static final int WIDTH = 28;
    private static final int HEIGHT = 50;
    private static final int JUMPING_LIMIT = 42;

    private Image imgMario;
    private boolean jumping;
    private int jumpingExtent;

    public Mario(int x, int y) {
        super(x, y, WIDTH, HEIGHT);
        this.imgMario = Utils.getImage(Res.IMG_MARIO_DEFAULT);
        this.jumping = false;
        this.jumpingExtent = 0;
    }

    public boolean isJumping() {
        return jumping;
    }

    public void setJumping(boolean jumping) {
        this.jumping = jumping;
    }

    public Image doJump() {
        String str;

        this.jumpingExtent++;

        if (this.jumpingExtent < JUMPING_LIMIT) {
            if (this.getY() > Platform.getPlatform().getHeightLimit()) {
                this.setY(this.getY() - 4);
            } else {
                this.jumpingExtent = JUMPING_LIMIT;
            }
            str = getCorrectNameResource(true);
        } else if (this.getY() + this.getHeight() < Platform.getPlatform().getFloorOffsetY()) {
            this.setY(this.getY() + 1);
            str = getCorrectNameResource(true);
        } else {
            str = getCorrectNameResource(false);
            this.jumping = false;
            this.jumpingExtent = 0;
        }

        return Utils.getImage(str);
    }

    private String getCorrectNameResource(boolean jump) {
        if (jump) {
            return this.isTurnedToRight() ? Res.IMG_MARIO_SUPER_DX : Res.IMG_MARIO_SUPER_SX;
        } else {
            return this.isTurnedToRight() ? Res.IMG_MARIO_ACTIVE_DX : Res.IMG_MARIO_ACTIVE_SX;
        }
    }

    public boolean contactPiece(Piece piece) {
        return this.isHitBackObject(piece) || this.isHitAboveObject(piece)
                || this.isHitAheadObject(piece) || this.isHitBelowObject(piece);
    }

    @Override
    public void move() {
        if (Platform.getPlatform().getxPos() >= 0) {
            this.setX(this.getX() - Platform.getPlatform().getMov());
        }
    }

    @Override
    public void contactCharacter(BasicCharacter character) {
        if (this.isHitAheadCharacter(character) || this.isHitBackCharacter(character)) {
            if (character.isAlive()) {
                this.setMoving(false);
                this.setAlive(false);
            } else {
                this.setAlive(true);
            }
        } else if (this.isHitBelowCharacter(character)) {
            character.setMoving(false);
            character.setAlive(false);
        }
    }

    @Override
    public void contactObject(GameObject object) {
        if (this.isHitAheadObject(object) && this.isTurnedToRight()
                || this.isHitBackObject(object) && !this.isTurnedToRight()) {
            Platform.getPlatform().setMov(0);
            this.setMoving(false);
        }

        if (this.isHitBelowObject(object) && this.jumping) {
            Platform.getPlatform().setFloorOffsetY(object.getY());
        } else if (!this.isHitBelowObject(object)) {
            Platform.getPlatform().setFloorOffsetY(FLOOR_OFFSET_Y_INITIAL);
            if (!this.jumping) {
                this.setY(MARIO_OFFSET_Y_INITIAL);
            }

            if (isHitAboveObject(object)) {
                Platform.getPlatform().setHeightLimit(object.getY() + object.getHeight()); // the new sky goes below the object
            } else if (!this.isHitAboveObject(object) && !this.jumping) {
                Platform.getPlatform().setHeightLimit(0); // initial sky
            }
        }
    }
}
