package code.model.characters;

import java.awt.Image;

import java.lang.*;

import code.model.objects.GameObject;
import code.utils.Res;
import code.utils.Utils;

public class BasicCharacter implements Character {

    private static final int PROXIMITY_MARGIN = 10;
    private static final int PROXIMITY_MARGIN_HIT = 5;
    private int width, height;
    private int x, y;
    private boolean moving;
    private boolean toRight;
    private int counter;
    private boolean alive;

    public BasicCharacter(int x, int y, int width, int height) {
        this.x = x;
        this.y = y;
        this.height = height;
        this.width = width;
        this.counter = 0;
        this.moving = false;
        this.toRight = true;
        this.alive = true;
    }

    @Override
    public int getX() {
        return x;
    }

    @Override
    public int getY() {
        return y;
    }

    @Override
    public int getWidth() {
        return width;
    }

    @Override
    public int getHeight() {
        return height;
    }

    @Override
    public int getCounter() {
        return this.counter;
    }

    @Override
    public boolean isAlive() {
        return alive;
    }

    @Override
    public boolean isMoving() {
        return this.moving;
    }

    @Override
    public boolean isTurnedToRight() {
        return toRight;
    }

    @Override
    public void setAlive(boolean alive) {
        this.alive = alive;
    }

    @Override
    public void setX(int x) {
        this.x = x;
    }

    @Override
    public void setY(int y) {
        this.y = y;
    }

    @Override
    public void setMoving(boolean moving) {
        this.moving = moving;
    }

    @Override
    public void setTurnedToRight(boolean toRight) {
        this.toRight = toRight;
    }

    @Override
    public void setCounter(int counter) {
        this.counter = counter;
    }

    @Override
    public Image walk(String name, int frequency) {
        String str = Res.IMG_BASE + name +
                (!this.moving || ++this.counter % frequency == 0 ?
                Res.IMGP_STATUS_ACTIVE :
                Res.IMGP_STATUS_NORMAL) +
                (this.toRight ? Res.IMGP_DIRECTION_DX : Res.IMGP_DIRECTION_SX) + Res.IMG_EXT;
        return Utils.getImage(str);
    }

    @Override
    public boolean isHitAheadObject(GameObject object) {
        return !(this.x + this.width < object.getX()
                || this.x + this.width > object.getX() + PROXIMITY_MARGIN_HIT
                || this.y + this.height <= object.getY()
                || this.y >= object.getY() + object.getHeight());
    }

    @Override
    public boolean isHitBackObject(GameObject object) {
        return !(this.x > object.getX() + object.getWidth()
                || this.x + this.width < object.getX() + object.getWidth() - PROXIMITY_MARGIN_HIT
                || this.y + this.height <= object.getY()
                || this.y >= object.getY() + object.getHeight());
    }

    @Override
    public boolean isHitBelowObject(GameObject object) {
        return !(this.x + this.width < object.getX() + PROXIMITY_MARGIN_HIT
                || this.x > object.getX() + object.getWidth() - PROXIMITY_MARGIN_HIT
                || this.y + this.height < object.getY()
                || this.y + this.height > object.getY() + PROXIMITY_MARGIN_HIT);
    }

    @Override
    public boolean isHitAboveObject(GameObject object) {
        return !(this.x + this.width < object.getX() + PROXIMITY_MARGIN_HIT
                || this.x > object.getX() + object.getWidth() - PROXIMITY_MARGIN_HIT
                || this.y < object.getY() + object.getHeight()
                || this.y > object.getY() + object.getHeight() + PROXIMITY_MARGIN_HIT);
    }

    @Override
    public boolean isHitAheadCharacter(Character character) {
        return this.isTurnedToRight() && !(this.x + this.width < character.getX()
                || this.x + this.width > character.getX() + PROXIMITY_MARGIN_HIT
                || this.y + this.height <= character.getY()
                || this.y >= character.getY() + character.getHeight());
    }

    @Override
    public boolean isHitBackCharacter(Character character) {
        return !(this.x > character.getX() + character.getWidth()
                || this.x + this.width < character.getX() + character.getWidth() - PROXIMITY_MARGIN_HIT
                || this.y + this.height <= character.getY()
                || this.y >= character.getY() + character.getHeight());
    }

    @Override
    public boolean isHitBelowCharacter(Character character) {
        return !(this.x + this.width < character.getX()
                || this.x > character.getX() + character.getWidth()
                || this.y + this.height < character.getY()
                || this.y + this.height > character.getY());
    }

    @Override
    public boolean isNearbyCharacter(Character character) {
        return ((this.x > character.getX() - PROXIMITY_MARGIN
                && this.x < character.getX() + character.getWidth() + PROXIMITY_MARGIN)
                || (this.x + this.width > character.getX() - PROXIMITY_MARGIN
                && this.x + this.width < character.getX() + character.getWidth() + PROXIMITY_MARGIN));
    }

    @Override
    public boolean isNearbyObject(GameObject object) {
        return ((this.x > object.getX() - PROXIMITY_MARGIN
                && this.x < object.getX() + object.getWidth() + PROXIMITY_MARGIN)
                || (this.getX() + this.width > object.getX() - PROXIMITY_MARGIN
                && this.x + this.width < object.getX() + object.getWidth() + PROXIMITY_MARGIN));
    }
}
