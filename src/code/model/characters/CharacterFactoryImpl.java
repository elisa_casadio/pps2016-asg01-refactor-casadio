package code.model.characters;

/**
 * Created by Elisa on 16/03/2017.
 */
public class CharacterFactoryImpl implements CharacterFactory {

    @Override
    public Character createMario(int x, int y) {
        return new Mario(x, y);
    }

    @Override
    public Character createMushroom(int x, int y) {
        return new Mushroom(x, y);
    }

    @Override
    public Character createTurtle(int x, int y) {
        return new Turtle(x, y);
    }
}
