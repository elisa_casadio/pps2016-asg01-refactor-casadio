package code.model.characters;

import code.model.objects.GameObject;

import java.awt.Image;

/**
 * An interface modelling a character of game.
 */
public interface Character {

	/**
	 *
	 * @return the X coordinate of character.
	 */
	int getX();

	/**
	 *
	 * @return the Y coordinate of character.
	 */
	int getY();

	/**
	 *
	 * @return the width of character.
	 */
	int getWidth();

	/**
	 *
	 * @return the height of character.
	 */
	int getHeight();

	/**
	 *
	 * @return the value of counter.
	 */
	int getCounter();

	/**
	 *
	 * @return true if character is alive, otherwise false.
	 */
	boolean isAlive();

	/**
	 *
	 * @return true if character is moving, otherwise false.
	 */
	boolean isMoving();

	/**
	 *
	 * @return true if character is turned right, otherwise false.
	 */
	boolean isTurnedToRight();

	/**
	 *
	 * @param alive true if character is alive, otherwise false.
	 */
	void setAlive(boolean alive);

	/**
	 *
	 * @param x the X coordinate of character.
	 */
	void setX(int x);

	/**
	 *
	 * @param y the Y coordinate of character.
	 */
	void setY(int y);

	/**
	 *
	 * @param moving true if the character is moving, otherwise false.
	 */
	void setMoving(boolean moving);

	/**
	 *
	 * @param toRight true if the character is turned to right, otherwise false.
	 */
	void setTurnedToRight(boolean toRight);

	/**
	 *
	 * @param counter the value of counter.
	 */
	void setCounter(int counter);

	/**
	 *
	 * @param name the name of character.
	 * @param frequency the frequency of character.
	 * @return the image of character.
	 */
	Image walk(String name, int frequency);

	/**
	 *
	 * @param object a game object
	 * @return true if a character hit ahead the object, otherwise false.
	 */
	boolean isHitAheadObject(GameObject object);

	/**
	 *
	 * @param character a character
	 * @return true if a character hit ahead the character, otherwise false.
	 */
	boolean isHitAheadCharacter(Character character);

	/**
	 *
	 * @param object a game object
	 * @return true if a character hit back the object, otherwise false.
	 */
	boolean isHitBackObject(GameObject object);

	/**
	 *
	 * @param character a character
	 * @return true if a character hit back the character, otherwise false.
	 */
	boolean isHitBackCharacter(Character character);

	/**
	 *
	 * @param object a game object
	 * @return true if a character hit below the object, otherwise false.
	 */
	boolean isHitBelowObject(GameObject object);

	/**
	 *
	 * @param character a character
	 * @return true if a character hit below the character, otherwise false.
	 */
	boolean isHitBelowCharacter(Character character);

	/**
	 *
	 * @param object a game object
	 * @return true if a character hit above the object, otherwise false.
	 */
	boolean isHitAboveObject(GameObject object);

	/**
	 *
	 * @param character a character
	 * @return true if a character is nearby the character, otherwise false.
	 */
	boolean isNearbyCharacter(Character character);

	/**
	 *
	 * @param object a game object
	 * @return true if a character is nearby the object, otherwise false.
	 */
	boolean isNearbyObject(GameObject object);
}
