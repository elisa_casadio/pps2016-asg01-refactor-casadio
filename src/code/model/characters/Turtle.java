package code.model.characters;

import java.awt.Image;

import code.utils.Res;
import code.utils.Utils;

public class Turtle extends Enemy {

    private static final int WIDTH = 43;
    private static final int HEIGHT = 50;

    private Image imageTurtle;

    public Turtle(int x, int y) {
        super(x, y, WIDTH, HEIGHT);
        this.imageTurtle = Utils.getImage(Res.IMG_TURTLE_IDLE);
    }

    public Image getImageTurtle() {
        return imageTurtle;
    }

    public Image deadImage() {
        return Utils.getImage(Res.IMG_TURTLE_DEAD);
    }
}
