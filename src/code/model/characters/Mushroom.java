package code.model.characters;

import java.awt.Image;

import code.utils.Res;
import code.utils.Utils;

public class Mushroom extends Enemy {

    private static final int WIDTH = 27;
    private static final int HEIGHT = 30;

    private Image imageMushroom;

    public Mushroom(int x, int y) {
        super(x, y, WIDTH, HEIGHT);
        this.imageMushroom = Utils.getImage(Res.IMG_MUSHROOM_DEFAULT);
    }

    public Image getImageMushroom() {
        return this.imageMushroom;
    }

    public Image deadImage() {
        return Utils.getImage(this.isTurnedToRight() ? Res.IMG_MUSHROOM_DEAD_DX : Res.IMG_MUSHROOM_DEAD_SX);
    }
}
