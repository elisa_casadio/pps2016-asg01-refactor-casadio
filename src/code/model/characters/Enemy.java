package code.model.characters;

import code.model.objects.GameObject;

import java.awt.*;

/**
 * Created by Elisa on 16/03/2017.
 */
abstract class Enemy extends BasicCharacter implements CharacterStrategy, Runnable {

    private static final int PAUSE = 15;

    private int offsetX;

    public Enemy(int x, int y, int width, int height) {
        super(x, y, width, height);
        this.setTurnedToRight(true);
        this.setMoving(true);
        this.offsetX = 1;

        Thread threadEnemy = new Thread(this);
        threadEnemy.start();
    }

    @Override
    public void move() {
        this.offsetX = isTurnedToRight() ? 1 : -1;
        this.setX(this.getX() + this.offsetX);
    }

    @Override
    public void contactCharacter(BasicCharacter character) {
        if (this.isHitAheadCharacter(character) && this.isTurnedToRight()) {
            setChangeEnemy(false, -1);
        } else if (this.isHitBackCharacter(character) && !this.isTurnedToRight()) {
            setChangeEnemy(true, 1);
        }
    }

    private void setChangeEnemy(boolean willBeRight, int offsetX) {
        this.setTurnedToRight(willBeRight);
        this.offsetX = offsetX;
    }

    @Override
    public void contactObject(GameObject object) {
        if (this.isHitAheadObject(object) && this.isTurnedToRight()) {
            setChangeEnemy(false, -1);
        } else if (this.isHitBackObject(object) && !this.isTurnedToRight()) {
            this.setTurnedToRight(true);
            setChangeEnemy(true, 1);
        }
    }

    @Override
    public void run() {
        while (this.isAlive()) {
            this.move();
            try {
                Thread.sleep(PAUSE);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    abstract public Image deadImage();
}