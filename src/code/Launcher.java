package code;

import code.controller.Refresh;

import javax.swing.*;

import code.view.FrameSingleton;
import code.view.Platform;

public class Launcher {

    private static final int WINDOW_WIDTH = 700;
    private static final int WINDOW_HEIGHT = 360;

    public static void main(String[] args) {
        FrameSingleton.getFrame().setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        FrameSingleton.getFrame().setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
        FrameSingleton.getFrame().setLocationRelativeTo(null);
        FrameSingleton.getFrame().setResizable(true);
        FrameSingleton.getFrame().setAlwaysOnTop(true);

        FrameSingleton.getFrame().setContentPane(Platform.getPlatform());
        FrameSingleton.getFrame().setVisible(true);

        Thread timer = new Thread(new Refresh());
        timer.start();
    }

}
